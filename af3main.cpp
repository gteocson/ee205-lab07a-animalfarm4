///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 3
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Geoffrey Teocson <gteocson@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   22_03_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <array>
#include <list>
#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;
using namespace animalfarm;



int main() {
   cout << "Welcome to Animal Farm 3" << endl;
   array<Animal*, 30> animalArray;
   animalArray.fill(NULL);
   

   cout << "Array of Animals" << endl;
   cout << "   Is it empty:" << boolalpha << animalArray.empty() << endl;
   cout << "   Number of elements:" << animalArray.size() << endl;
   cout << "   Max Size:" << animalArray.max_size() << endl; 

   for (int i=0; i < 25; i++) {
   
      animalArray[i] = AnimalFactory::getRandomAnimal();

   }

   cout << endl;

   for (int i=0; i < 25; i++) {

      cout << animalArray[i]->speak() << endl;

   }


   for(Animal* animal : animalArray){

      if(animal != NULL){
            delete animal;

      }

   }



   list<Animal*> animalList;
   for(int i = 0; i < 25; i++){
      animalList.push_front(AnimalFactory::getRandomAnimal());
      
            }


   cout << endl << "List of Animals" << endl;
   cout << "   Is it empty:" << boolalpha << animalList.empty() << endl;
   cout << "   Number of elements:" << animalList.size() << endl;
   cout << "   Max Size:" << animalList.max_size() << endl;
   
   for (Animal* animal : animalList) {

     cout << animal->speak() << endl;
   }

   for(Animal* animal : animalList){

      if(animal != NULL){
            delete animal;

      }

   }











}
