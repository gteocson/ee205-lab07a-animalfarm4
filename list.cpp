///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file list.cpp
/// @version 1.0
///
/// Single linked list functions
///
/// @author Geoffrey Teocson <gteocson@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   30_03_2021
///////////////////////////////////////////////////////////////////////////////

#include "list.hpp"
#include "node.hpp"
#include <cstdlib>


using namespace std;

namespace animalfarm {

const bool SingleLinkedList::empty() const{

   if(head == nullptr)
      return true;
   else
      return false;

}

void SingleLinkedList::push_front( Node* newNode ){

   Node* temp;
   temp = newNode;
   temp->next=head;
   head = temp;

}

Node* SingleLinkedList::pop_front(){

   Node* temp;
   if(head == nullptr)
      return nullptr;
   else{
      temp = head;
      head=head->next;
      return temp;

      
   }
}

Node* SingleLinkedList::get_first() const{

   return head;


}

Node* SingleLinkedList::get_next( const Node* currentNode ) const{


   return currentNode->next;

}

unsigned int SingleLinkedList::size() const{


   Node* temp;
   int i=0;
   temp=head;
   
   while( temp  != nullptr){
      i++;
      
      temp=temp->next;
      
   }
     
   return i;

}

}
