///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file list.hpp
/// @version 1.0
///
/// Header file for list
///
/// @author Geoffrey Teocson <gteocson@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   30_03_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "node.hpp"

using namespace std;

namespace animalfarm {

class SingleLinkedList{

public:

   Node* next;

   const bool empty() const;


   void push_front( Node* newNode );


   Node* pop_front();


   Node* get_first() const;


   Node* get_next( const Node* currentNode ) const;


   unsigned int size() const;

protected:

   Node* head = nullptr;

};
}

