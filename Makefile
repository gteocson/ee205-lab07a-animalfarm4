###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 07a - Animal Farm 4
#
# @file    Makefile
# @version 1.0
#
# @author Geoffrey Teocson <gteocson@hawaii.edu>
# @brief  Lab 05a - Animal Farm 2 - EE 205 - Spr 2021
# @date   30_03_2021
###############################################################################

all: main af3main

main.o:  animal.hpp main.cpp
	g++ -c main.cpp

af3main.o:	animal.hpp af3main.cpp
	g++ -c af3main.cpp

animal.o: animal.hpp animal.cpp 
	g++ -c animal.cpp
	
mammal.o: mammal.hpp mammal.cpp animal.hpp
	g++ -c mammal.cpp

fish.o: fish.hpp fish.cpp animal.hpp
	g++ -c fish.cpp

bird.o: bird.hpp bird.cpp animal.hpp
	g++ -c bird.cpp

cat.o: cat.cpp cat.hpp mammal.hpp
	g++ -c cat.cpp

dog.o: dog.cpp	dog.hpp mammal.hpp
	g++ -c dog.cpp

nunu.o: nunu.cpp nunu.hpp fish.hpp
	g++ -c nunu.cpp

aku.o: aku.cpp aku.hpp fish.hpp
	g++ -c aku.cpp

palila.o: palila.cpp palila.hpp bird.hpp
	g++ -c palila.cpp

nene.o: nene.cpp nene.hpp bird.hpp
	g++ -c nene.cpp

node.o: node.cpp node.hpp
	g++ -c node.cpp

list.o: list.cpp list.hpp
	g++ -c list.cpp

main: main.cpp *.hpp main.o animal.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o node.o list.o
	g++ -o main main.o animal.o mammal.o fish.o cat.o bird.o dog.o nunu.o aku.o palila.o nene.o node.o list.o

af3main: af3main.cpp *.hpp af3main.o animal.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o
	g++ -o af3main af3main.o animal.o mammal.o fish.o cat.o bird.o dog.o nunu.o aku.o palila.o nene.o

clean:
	rm -f *.o main af3main
