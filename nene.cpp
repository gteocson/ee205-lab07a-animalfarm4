///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nene.cpp
/// @version 1.0
///
/// Exports data about all nene birds
///
/// @author Geoffrey Teocson <gteocson@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   26_02_2021
///////////////////////////////////////////////////////////////////////////////

#include<string>
#include<iostream>

#include "nene.hpp"

using namespace std;

namespace animalfarm {

Nene::Nene( string getTag, enum Color newColor, enum Gender newGender ){
   gender = newGender;
   tagID = getTag;
   species = "Branta sandvicensis";
   isMigratory = 1;
   featherColor = newColor;
}

const string Nene::speak() {
   return string ("Nay, nay");
}


void Nene::printInfo() {
   cout << "Nene" << endl;
   cout << "   Tag ID = [" << tagID << "]" << endl;
   Bird::printInfo();
}

}
